
const mongoose = require('mongoose');
  
const studentSchema = new mongoose.Schema({
    _id:mongoose.Schema.Types.ObjectId,
   
    email:String,
    firstName:String,
    lastName:String,
    mobile:Number,
    password:String

    
})

module.exports = mongoose.model('Student',studentSchema);