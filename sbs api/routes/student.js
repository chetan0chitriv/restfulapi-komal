const express = require('express');
const router = express.Router();
const Student = require('../model/student');
const mongoose = require('mongoose');
const checkAuth=require('../middleware/check-auth');

// get request

router.get('/',checkAuth,
(req,res,next)=>{
                                
    Student.find()
   .then(result=>{
       res.status(200).json({
           studentData:result
       });
   })
   .catch(err=>{
       console.log(err);
       res.status(500).json({
           error:err
       })
   });
})

// get by id request
    router.get('/:id',(req,res,next)=>{

        console.log(req.params.id);
    Student.finallyId(req.params.id)
    .then(result=>{
        res.status(200).json({
            student:result
        })
    })
    .catch(err=>{
console.log(err);
res.status(500).json({
    error:err
   })
 })
})

// post request
router.post('/',(req,res,next)=>{

    const student = new Student({
      _id:new mongoose.Types.ObjectId,
      email:req.body.email,
      firstName:req.body.firstName,
      lastName:req.body.lastName,
      mobile:req.body.mobile,
      password:req.body.password
    })
    student.save()
    .then(result=>{
        console.log(result);
        res.status(200).json({
        newStudent:"data added successfully"
       })
    
    })
    .catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err

        })
    })
})

// delete request
router.delete('/:id',(req,res,next)=>{

    Student.remove({_id:req.params.id})
    .then(result=>{
        res.status(200).json({
     message:'student delectd',
     result:result
    })

   })

   .catch(err=>{
       res.status(500).json({
           error:err
       })
   })
})


// put request
router.put('/:id',(req,res,next)=>{

     console.log(req.params.id);
    Student.findOneAndUpdate({_id:req.params.id},{
   $set:{
    name:req.body.name,
    gender:req.body.gender,
    email:req.body.email
   }
    })
    .then(result=>{
      res.status(200).json({
          updated_student:result
      })
    })
    .catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        })
    })
})

module.exports = router;



