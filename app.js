const express = require('express');
const app = express();
const studentRoute=require('./sbs api/routes/student');
const facultyRoute = require('./sbs api/routes/faculty');
const userRoute = require('./sbs api/routes/user');
const bodyParser=require('body-parser');
const mongoose = require('mongoose');
mongoose.connect('mongodb+srv://sbs:komal@cluster0.uytjr.mongodb.net/myFirstDatabase?retryWrites=true&w=majority');


mongoose.connection.on('error',err=>{
    console.log('connection failed');
});

mongoose.connection.on('connected',connected=>{
    console.log('connected with database....');
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extented: false
}));
app.use('/student',studentRoute);
app.use('/faculty',facultyRoute);
app.use('/user',userRoute);

//  app.use((req,res,next)=>{
//     res.status(200).json({
//         message:'app is running'

app.use((req,res,next)=>{
        res.status(404).json({
            error:'bad request'
    })
})

module.exports=app;